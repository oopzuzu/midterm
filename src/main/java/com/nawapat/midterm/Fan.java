package com.nawapat.midterm;

public class Fan {
    public static void main(String[] args) {
        FanClass brand1 = new FanClass();
        FanClass brand2 = new FanClass();
        FanClass brand3 = new FanClass();
        FanClass doyoung = new FanClass();

        brand1.setBrand("Hatari");
        brand1.setModel("HT-S18M2");
        brand1.setColor("Blue");
        brand1.setPrice(1198);

        brand2.setBrand("SHARP");
        brand2.setModel("PJ-TA181");
        brand2.setColor("Purple");
        brand2.setPrice(1500);

        brand3.setBrand("Mitsubishi Electric");
        brand3.setModel("R16-GZ-PP");
        brand3.setColor("Purple");
        brand3.setPrice(1290);

        brand1.printBrand();
        System.out.println();
        brand2.printBrand();
        System.out.println();
        brand3.printBrand();
        System.out.println();
        
        doyoung.setBuy("Doyoung", 2000, "Hatari", 1198);
        doyoung.printBuy();

    }
}
