package com.nawapat.midterm;

public class FishClass {
    private String name;
    private int hp;
    private int heal;

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setHP(int hp) {
        this.hp = hp;
    }

    public int getHP() {
        return hp;
    }

    public int Heal() {
        heal = hp + 100;
        return heal;
    }

    public void print() {
        System.out.println("Name : " + name + " /HP : " + hp);
        System.out.println("++Heal : 'HP' ++100");
        System.out.println("Name : " + name + " /++HP : " + Heal());

    }

}
