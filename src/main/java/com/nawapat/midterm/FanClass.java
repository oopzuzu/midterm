package com.nawapat.midterm;

public class FanClass {
    String name;
    double balance;
    String brand;
    String model;
    String color;
    double price;
    double change;

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public double getBalance() {
        return balance;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getBrand() {
        return brand;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getModel() {
        return model;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getColor() {
        return color;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getPrice() {
        return price;
    }

    public void printBrand() {
        System.out.println("Brand : " + brand);
        System.out.println("Model : " + model);
        System.out.println("Color : " + color);
        System.out.println("Price : " + price + " Baht");

    }

    public double setBuy(String name, double balance, String brand, double price) {
        this.name = name;
        this.balance = balance;
        this.brand = brand;
        this.price = price;
        change = balance - price;
        return change;
    }

    public void printBuy() {
        System.out.println(name + " Buy " + brand);
        System.out.println("Total : " + price + " Baht");
        System.out.println("Cash : " + balance + " Baht");
        System.out.println("Chang : " + change + " Baht");

    }
}
