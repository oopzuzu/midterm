package com.nawapat.midterm;

public class Fish {
    public static void main(String[] args) {
        FishClass fish1 = new FishClass();
        FishClass fish2 = new FishClass();
        FishClass fish3 = new FishClass();
        fish1.setName("CLOWNFISH");
        fish1.setHP(50);
        fish2.setName("SWORDFISH");
        fish2.setHP(250);
        fish3.setName("HAMMERHEAD SHARK");
        fish3.setHP(500);

        fish1.print();
        System.out.println();
        fish2.print();
        System.out.println();
        fish3.print();



    }
}
